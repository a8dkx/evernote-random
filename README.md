# Evernote Random

View a random note every day.

1. Open `RandomEvernote.applescript` in the ScriptEditor (Mac).
1. Edit line 4 to choose the Evernote stacks to include.
1. Run the script to open a random note from those stacks.

You may also consider exporting the script to an Application for ease-of-use.